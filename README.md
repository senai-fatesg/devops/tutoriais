## Cria a chave

```bash
C:\AmbienteTrabalho\Aplicativos\jdk1.8.0_151\bin\keytool -genkey -v -keystore release-key.jks -keyalg RSA -keysize 2048 -validity 10000 -alias nome_app
```

## Assina o apk (stage do pipeline)
   
```bash
C:\AmbienteTrabalho\Aplicativos\jdk1.8.0_151\bin\jarsigner -verbose -sigalg SHA1withRSA -digestalg SHA1 -keystore release-key.jks C:\AmbienteTrabalho\Projetos\ifag\app\nome_app\platforms\android\app\build\outputs\apk\release\app-release-unsigned.apk nome_app
```

```bash
del C:\AmbienteTrabalho\Projetos\ifag\app\nome_app\platforms\android\app\build\outputs\apk\release\app-release-final.apk
```

```bash
C:\AmbienteTrabalho\Aplicativos\android-sdk\build-tools\23.0.2\zipalign -v 4 C:\AmbienteTrabalho\Projetos\ifag\app\nome_app\platforms\android\app\build\outputs\apk\release\app-release-unsigned.apk C:\AmbienteTrabalho\Projetos\ifag\app\nome_app\platforms\android\app\build\outputs\apk\release\app-release-final.apk
```

## Acesse o diretório
```bash
explorer C:\AmbienteTrabalho\Projetos\ifag\app\nome_app\platforms\android\app\build\outputs\apk\release\
```


