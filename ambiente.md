# Instalação do ambiente

## 1. Instalar do docker

https://www.digitalocean.com/community/tutorials/como-instalar-e-usar-o-docker-no-ubuntu-18-04-pt

```bash
apt update
apt install apt-transport-https ca-certificates curl software-properties-common
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu bionic stable"
apt update
apt install docker-ce
```


## 2. Executar o sonar:

```bash
docker run -d --name sonar -p 30001:9000 sonarqube
```

# 3. Instalar o jenkins

* baixar http://mirrors.jenkins.io/war-stable/latest/jenkins.war

* executar:

```bash
nohup /opt/java/bin/java -Duser.home=/opt/jenkins/ -jar jenkins.war --httpPort=30002 --prefix=/jenkins &
```